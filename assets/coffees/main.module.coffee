config = ($locationProvider, $urlRouterProvider) ->
  $locationProvider.hashPrefix '!'
  $urlRouterProvider.otherwise '/'

dependencies = [
  'ui.router'
]

angular.module 'main', dependencies
  .config ['$locationProvider', '$urlRouterProvider', config]
