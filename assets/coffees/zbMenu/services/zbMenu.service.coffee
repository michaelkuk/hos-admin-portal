menuService = () ->
  items = []
  service =
    addItem: (item) ->
      items.push item
      return @

    getItems: ()->
      items

  return service

angular.module 'zbMenu'
  .factory '$zbMenu', [menuService]
