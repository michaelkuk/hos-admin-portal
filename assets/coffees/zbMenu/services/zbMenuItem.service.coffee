menuItem = ($zbMenu)->
  class zbMenuItem
    constructor: (config)->
      @_children = []

      @_priority = config.priority
      @_text = config.text
      @_state = config.state unless !config.state
      @_href = config.href unless !config.href
      @_icon = config.icon

    setText: (text)->
      throw new Error('Parameter must be a string') unless typeof text == 'string'
      @_text = text
      return @

    setState: (state) ->
      throw new Error('Parameter must be a string') unless typeof state == 'string'
      @_state = state
      return @

    setPriority: (priority) ->
      throw new Error('Parameter must be a number') unless typeof priority == 'number'
      @_priority = priority
      return @

    addChild: (child)->
      throw new Error('Parameter must be an instance of zbMenuItem class') unless child instanceof zbMenuItem
      @_children.push(child)
      return @

    addChildren: (children)->
      throw new Error('Parameter must be an array') unless Array.isArray(children)

      for child in children
        throw new Error('Item is not instance of $$zbMenuItem class') unless child instanceof zbMenuItem
        @_children.push(child)
      return @

    save: ()->
      $zbMenu.addItem(@)
      return

  return zbMenuItem

angular.module('zbMenu').factory('$$zbMenuItem', ['$zbMenu', menuItem])
