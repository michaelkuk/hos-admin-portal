zbMenuItemFactory = ($$zbMenuItem)->
  factory = (config = {})->
    config.icon ?= ''

    # Check whether link destination is defined
    throw new Error('"state" or "href" property must be defined') if (
      typeof config.state isnt 'string' and typeof config.href isnt 'string')

    # Assign priority unless it was assigned
    config.priority ?= 100

    # Throw an error if priority was assigned and is not a number
    throw new Error('"priority" property must be a number or undefined') if (
      typeof config.priority isnt 'number')

    # Throw an error if link text is not specified
    throw new Error('"text" property must be a string') if typeof config.text isnt 'string'

    # Return new zbMenuItem instance
    return new $$zbMenuItem(config)

  return factory


angular.module('zbMenu')
  .factory('$zbMenuItem', ['$$zbMenuItem', zbMenuItemFactory])
