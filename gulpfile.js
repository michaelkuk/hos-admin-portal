/* jshint node:true */
'use strict';

/* Require Modules */
/* ========================================================================== */
var
  gulp = require('gulp'),
  uglify = require('gulp-uglify'),
  usemin = require('gulp-usemin'),
  htmlmin = require('gulp-htmlmin'),
  sass = require('gulp-sass'),
  connect = require('gulp-connect'),
  rename = require('gulp-rename'),
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
  concat = require('gulp-concat'),
  coffee = require('gulp-coffee'),
  Server = require('karma').Server,
  runSequence = require('run-sequence'),
  templatecache = require('gulp-angular-templatecache');

/* Load Config File */
/* ========================================================================== */

var config = require('./gulp-config.json');

/* Persistence variables */
/* ========================================================================== */
var serverInstance;

/* Define Atomic Tasks */
/* ========================================================================== */

// Compile SASS
gulp.task('custom-styles', function() {
  return gulp
    .src(config.paths.mainStyle)
    .pipe(sass(config.sass).on('error', sass.logError))
    .pipe(rename({
      basename: 'app.min'
    }))
    .pipe(gulp.dest(config.paths.dest));
});

// Compile JavaScript
gulp.task('custom-js', function() {
  return gulp
    .src(config.paths.scripts)
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.paths.dest));
});

// Compile CoffeeScript
gulp.task('custom-coffee', function() {
  return gulp
    .src(config.paths.coffees)
    .pipe(coffee())
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.paths.dest));
});

// Error-Check JavaScript
gulp.task('jshint', function() {
  return gulp
    .src(config.paths.scripts)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe(jshint.reporter('fail'));
});

// Copy Images
gulp.task('custom-images', function() {
  return gulp
    .src(config.paths.images)
    .pipe(rename({
      dirname: 'img'
    }))
    .pipe(gulp.dest(config.paths.dest));
});

// Copy Fonts
gulp.task('bower-fonts', function() {
  return gulp
    .src(config.paths.fonts)
    .pipe(rename({
      dirname: 'fonts'
    }))
    .pipe(gulp.dest(config.paths.dest));
});

// Compile templates
gulp.task('custom-templates', function() {
  return gulp
    .src(config.paths.templates)
    .pipe(htmlmin(config.htmlmin))
    .pipe(templatecache(config.templatecache))
    .pipe(uglify())
    .pipe(rename({
      basename: 'tpls.min',
      dirname: '/'
    }))
    .pipe(gulp.dest(config.paths.dest));
});

// Compile index.html
gulp.task('usemin', function() {
  return gulp
    .src(config.paths.index)
    .pipe(usemin({
      html: [htmlmin(config.htmlmin)]
    }))
    .pipe(gulp.dest(config.paths.dest));
});

// Create Local Webserver
gulp.task('server', function() {
  return connect
    .server({
      root: config.paths.dest,
      livereload: config.server.livereload,
      port: config.server.port
    });
});

// Enable Live Reload on Change
gulp.task('livereload', function() {
  return gulp
    .src(config.paths.dest)
    .pipe(connect.reload());
});

gulp.task('ci-test', function(done) {
  if(serverInstance){
    done();
  } else{
    serverInstance = new Server({
      configFile: __dirname + '/karma.conf.js',
      singleRun: false,
      autoWatch: true
    });
    serverInstance.on('browser_register', function(){
      done();
    });
    serverInstance.start();
  }
});

gulp.task('test', function(done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true,
    autoWatch: false
  }, done).start();
});

// Trigger tasks on change
gulp.task('watch', function() {
  gulp.watch([config.paths.coffees], ['custom-coffee']);
  gulp.watch([config.paths.styles], ['custom-styles']);
  gulp.watch([config.paths.images], ['custom-images']);
  gulp.watch([config.paths.templates], ['custom-templates']);
  gulp.watch([config.paths.index], ['usemin']);
  gulp.watch([config.paths.dest + '/**/*'], ['livereload']);
});

/* Composite Tasks */
/* ========================================================================== */

// Build Custom Sources
gulp.task('build-custom', ['custom-coffee', 'custom-styles', 'custom-images', 'custom-templates', 'usemin']);

// Bower components
gulp.task('build-components', ['bower-fonts']);

// Build
gulp.task('build-dev', ['build-custom', 'build-components']);

// Build for production
gulp.task('build-production', function(){
  runSequence('test', 'build-dev');
});

// Default Task
gulp.task('default', ['ci-test', 'build-dev', 'server', 'watch']);
