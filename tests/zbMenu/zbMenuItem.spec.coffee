describe 'Module: zbMenu', ()->
  beforeEach module 'zbMenu'


  describe 'Factory: $$zbMenuItem', () ->
    beforeEach inject ($zbMenu, $$zbMenuItem)->
      @zbMenu = $zbMenu
      @zbMenuItem = $$zbMenuItem
    beforeEach ()->
      @params =
        priority: 100
        text: "link1"
        state: 'some.state'
        icon: 'fa fa-check'
      @instance = new @zbMenuItem @params

    describe 'Constructor', ()->
      it "Should Instantiate properly", ()->
        expect(@instance instanceof @zbMenuItem).toBe(true)

      it "Should expose passed properties on the object", ()->
        expect(@instance._priority).toBe(@params.priority)
        expect(@instance._text).toBe(@params.text)
        expect(@instance._state).toBe(@params.state)
        expect(@instance._icon).toBe(@params.icon)

    describe 'setText', ()->
      it "should return itself for fluent API", ()->
        expect(@instance.setText('new text')).toBe(@instance)

      it "should store the argument of this function as text of the link", ()->
        expect(@instance.setText('new text')._text).toBe('new text')

      it "should throw an error if input is not string", ()->
        expect(()=> @instance.setText(123)).toThrow()
        expect(()=> @instance.setText()).toThrow()
        expect(()=> @instance.setText(null)).toThrow()

    describe 'setState', ()->
      it 'Should return itself for fluent API', ()->
        expect(@instance.setState('state1.index')).toBe(@instance)

      it 'should store argument as target state of the link', ()->
        expect(@instance.setState('state1.index')._state).toBe('state1.index')

      it 'should throw an error if argument is not a string', ()->
        expect(()=> @instance.setState(123)).toThrow()
        expect(()=> @instance.setState()).toThrow()
        expect(()=> @instance.setState(null)).toThrow()

    describe 'setPriority', ()->
      it 'Should return itself for fluent API', ()->
        expect(@instance.setPriority(1)).toBe(@instance)

      it 'should store argument as priority of the link', ()->
        expect(@instance.setPriority(2)._priority).toBe(2)

      it 'should throw an error if argument is not a string', ()->
        expect(()=> @instance.setPriority('123')).toThrow()
        expect(()=> @instance.setPriority()).toThrow()
        expect(()=> @instance.setPriority(null)).toThrow()

    describe 'addChild', ()->
      beforeEach ()->
        @children = []

        for i in [1..10]
          params =
            priority: Math.floor(Math.random() * 6) + 1
            text: "child#{i}"
            state: "state.child#{i}"
            icon: "fa fa-cross"
          @children.push(new @zbMenuItem(params))

      it 'Should return itself for fluent API', ()->
        expect(@instance.addChild(@children[0])).toBe(@instance)

      it 'should store argument as priority of the link', ()->
        expect(@instance.addChild(@children[0])._children[0]).toBe(@children[0])

      it 'should throw an error if argument is not a an instance of $$zbMenuItem', ()->
        expect(()=> @instance.addChild('123')).toThrow()
        expect(()=> @instance.addChild()).toThrow()
        expect(()=> @instance.addChild({})).toThrow()
        expect(()=> @instance.addChild(123)).toThrow()
        expect(()=> @instance.addChild(null)).toThrow()

      it 'should store multiple children if added', ()->
        @instance.addChild(@children[0]).addChild(@children[1]).addChild(@children[2])
        expect(@instance._children.length).toBe(3)

      it 'should store the children in order they were added', ()->
        @instance.addChild(@children[0]).addChild(@children[1]).addChild(@children[2])
        expect(@instance._children[0]).toBe(@children[0])
        expect(@instance._children[1]).toBe(@children[1])
        expect(@instance._children[2]).toBe(@children[2])

    describe 'addChildren', ()->
      beforeEach ()->
        @children = []

        for i in [1..10]
          params =
            priority: Math.floor(Math.random() * 6) + 1
            text: "child#{i}"
            state: "state.child#{i}"
            icon: "fa fa-cross"
          @children.push(new @zbMenuItem(params))

      it 'should return itself for fluent API', ()->
        expect(@instance.addChildren(@children)).toBe(@instance)

      it 'should store items in parameter array in an instance variable _children', ()->
        @instance.addChildren(@children)
        expect(@instance._children[0]).toBe(@children[0])
        expect(@instance._children[1]).toBe(@children[1])
        expect(@instance._children[2]).toBe(@children[2])

      it 'should throw an error if the input is not an array', ()->
        thrower1 = ()=>
          @instance.addChildren(1)
        thrower2 = ()=>
          @instance.addChildren('1')
        thrower3 = ()=>
          @instance.addChildren()
        thrower4 = ()=>
          @instance.addChildren({hello: 'world'})

        expect(thrower1).toThrow()
        expect(thrower2).toThrow()
        expect(thrower3).toThrow()
        expect(thrower4).toThrow()

      it 'should throw an error if input array contains object that is not instance of $$zbMenuItem', ()->
        thrower = ()=>
          @instance.addChildren([{hello: 'world'}])
        thrower1 = ()=>
          @instance.addChildren([@children[0], 1])
        thrower2 = ()=>
          @instance.addChildren([null, @children[0]])
        thrower3 = ()=>
          @instance.addChildren([undefined, @children[0]])

        expect(thrower).toThrow()
        expect(thrower1).toThrow()
        expect(thrower2).toThrow() 
        expect(thrower3).toThrow()
